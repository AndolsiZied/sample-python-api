import os
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Greeting(Resource):
    def get(self):
        return {'message': 'This API is running in {} environment.'.format(os.getenv('ENVIRONMENT'))}

api.add_resource(Greeting, '/')

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")