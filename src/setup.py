from setuptools import setup, find_packages


setup(
    name='sample-python-api',
    version='0.1',
    description='Sample Python API',
    long_description='README.md',
    packages=find_packages(exclude=('tests', 'docs'))
)