import os

import pytest

from greeting.api import app


@pytest.fixture
def client():
    with app.test_client() as client:
        yield client


@pytest.fixture
def environment():
    os.environ['environment'] = 'Test'


def test_given_all_params_when_create_app_expected_success(client, environment):
    response = client.get('/')
    assert 'This API is running in {} environment.'.format(environment).encode() in response.data
