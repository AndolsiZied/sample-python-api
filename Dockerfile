FROM python:3.6

COPY entrypoint.sh .
RUN chmod +x entrypoint.sh

COPY src /app/src

ENTRYPOINT ["/entrypoint.sh"]