#!/bin/bash

set -e

cd /app
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install -r src/requirements.txt
pip install -e src

source venv/bin/activate
python src/greeting/api.py