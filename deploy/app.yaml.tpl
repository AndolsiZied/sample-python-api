apiVersion: apps/v1
kind: Deployment
metadata:
  name: sample-python-api
  labels:
    app: sample-python-api
spec:
  replicas: 1
  selector:
    matchLabels:
      app: sample-python-api
  template:
    metadata:
      labels:
        app: sample-python-api
    spec:
      containers:
      - name: sample-python-api
        image: DOCKER_IMAGE
        env:
        - name: ENVIRONMENT
          value: "ENV_VALUE"
        ports:
        - containerPort: 5000

---
apiVersion: v1
kind: Service
metadata:
  name: svc-sample-python-api
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 5000
  type: LoadBalancer
  selector:
    app: sample-python-api
